#!/bin/ksh

#
#       $Id: vcsstatechk,v 1.1 2003/05/12 21:44:19 abidk Exp abidk $
#

#
# DO NOT EDIT THIS SCRIPT!!!
#
# All changes will be lost with the next release.  Use RCS
# to version control and release ALL changes.
#

# Sanity check
[[ -x /usr/bin/perl ]] &&
   [[ -n `/usr/bin/perl -v|head -2|grep v5.` ]] &&
   exec /usr/bin/perl -x $0

print "No perl is good perl." 1>&2

exit 255

#!perl -w

#
#       Last edited by:  $Author: abidk $
#                   on:  $Date: 2003/05/12 21:44:19 $
#             Filename:  $RCSfile: vcsstatechk,v $
#             Revision:  $Revision: 1.1 $
#

#
# vcsstatechk - VCS State Check
#
# A script to alert if a VCS service group is no longer online on the
# primary system.
#

#
# Revision History
# ---------------------------------------------
# Revision	Author		Comments/Change
# --------	------		---------------
# 1.1		Abid Khwaja	Initial release.
# 1.2		Abid Khwaja	Add code to output all-is-well message.
#
# Assumptions
# -----------
# - maximum of 2 nodes in cluster
# - VCS $maincf config file is in sync with output of @hastatus
# - VCS $maincf config file format lists service group name first and
#   list of machines hosting service group afterwards
# - output format of @hastatus command is fixed listing group state lines
#   beginning with "B"
#

use warnings;
use Fcntl;

#my $maincf = "/etc/VRTSvcs/conf/config/main.cf";
my $maincf = "/home/abidk/tmp/main.cf";
#my @hastatus = `/opt/VRTSvcs/bin/hastatus -sum`;
my @hastatus = `cat /home/abidk/tmp/main.out`;

sysopen(VCSCF, $maincf, O_RDONLY)
   || die "can't open $maincf: $!\n";

while (<VCSCF>) {
   if (/^group/) {
      @group = /\s+\b(\w+)\b/;
   } elsif (/SystemList/) {
      @groupsys{@group} = /{\s+\b(\w+)\b/;
   }
}

close(VCSCF)
   || die "can't close $maincf: $!\n";

while ( ($group, $system) = each %groupsys ) {
   foreach $line (@hastatus) {
      next unless $line =~ /^B\s/;
      if ($line =~ /($group)\s+($system)/) {
         unless ($line =~ /ONLINE/) {
            print "$group is no longer online on $system!!!\n";
            $allok++;
         }
         last;
      }
   }
}

print "ok\n" unless defined($allok);